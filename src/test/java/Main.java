import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Main {

    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver","C:/Driver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }

    @After
    public void fechar() {
        driver.quit();
    }

    @Test
    public void preencherDados() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("Arthur");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[2]/td/input")).sendKeys("123");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[3]/td/textarea")).sendKeys("Teste");
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[1]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[1]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[1]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click();
        Thread.sleep(3000);

        Assert.assertEquals("Arthur", driver.findElement(By.id("_valueusername")).getText());
        Assert.assertEquals("123", driver.findElement(By.id("_valuepassword")).getText());
        Assert.assertEquals("Comments...Teste", driver.findElement(By.id("_valuecomments")).getText());
        Assert.assertEquals("Hidden Field Value", driver.findElement(By.id("_valuehiddenField")).getText());
        Assert.assertEquals("cb1", driver.findElement(By.id("_valuecheckboxes0")).getText());
        Assert.assertEquals("rd1", driver.findElement(By.id("_valueradioval")).getText());
        Assert.assertEquals("ms1", driver.findElement(By.id("_valuemultipleselect0")).getText());
        Assert.assertEquals("dd3", driver.findElement(By.id("_valuedropdown")).getText());
        Assert.assertEquals("submit", driver.findElement(By.id("_valuesubmitbutton")).getText());
        Thread.sleep(3000);

    }
}
